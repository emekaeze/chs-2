/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var compApp = angular.module('compApp',['ngRoute','ui.bootstrap']).
config(['$routeProvider',function($routeProvider){
	$routeProvider.when('/dashboard-eedc', {templateUrl: '../disco/dashboard-eedc.html', controller: 'dashboardEedcCtrl'});
        $routeProvider.when('/complaint-log', {templateUrl: '../disco/complaint-log.html', controller: 'dashboardCtrl'});
	$routeProvider.when('/disco-upload', {templateUrl: '../disco/disco-upload.html', controller: 'discoUploadCtrl'});
	$routeProvider.when('/inbox', {templateUrl: '../disco/inbox.html', controller: 'inboxCtrl'});  
	$routeProvider.when('/compose', {templateUrl: '../disco/compose.html', controller: 'composeCtrl'}); 
	$routeProvider.when('/timeline', {templateUrl: '../disco/timeline.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/complaint-form-disco', {templateUrl: '../disco/complaint-form-disco.html', controller: 'complaintFormCtrl'});
	$routeProvider.when('/timeline3', {templateUrl: '../disco/timeline3.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline4', {templateUrl: '../disco/timeline4.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline5', {templateUrl: '../disco/timeline5.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline6', {templateUrl: '../disco/timeline6.html', controller: 'discoTimelineCtrl'});
        $routeProvider.otherwise({redirectTo: '/dashboard-eedc', controller: 'dashboardCtrl'});

}]);

