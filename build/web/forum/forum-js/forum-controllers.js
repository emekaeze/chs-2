/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

compApp.controller('dashboardCtrl',function($scope,$http){
	$(document).ready(function(){

				var table = $('#example').DataTable( {
						dom: '<"top"fl>rt<T>rt<"bottom"ip><"clear">',
						tableTools: {
							"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
						},
						ordering: true,
						"iDisplayLength": 5,
						"aoColumnDefs": [{ "bVisible": false, "aTargets": [3] }]
						
					} );
	
					yadcf.init(table , [{column_number : 1},
										{column_number : 2},
										{column_number : 3,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
					});
					
    
});

compApp.controller('complaintFormCtrl',function($scope,$http){
												
	$scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
	
});

compApp.controller('discoUploadCtrl',function($scope,$http){
	
});
compApp.controller('discoTimelineCtrl',function($scope,$http){
	
});

compApp.controller('inboxCtrl',function($scope,$http){
										
	$(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		}
	} );

    yadcf.init(table , [{column_number : 4,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });

});


compApp.controller('dashboardForumCtrl',function($scope,$http){
        
        var chart1;
 // globally available 
  $(document).ready(function () {
     $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Appeals',
                x: -20 //center
            },
            subtitle: {
                text: 'For: All DISCOs',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Appeals'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Appeal(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Appeals',
                data: [0.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 2600.0]
            }]
        });
   });
});

});


compApp.controller('composeCtrl',function($scope,$http,$location){
    $scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
});

compApp.controller('parentController',function($scope){   
    
    $scope.loading = false;
    $scope.$on('$routeChangeStart', function() {
      $scope.loading = true;
    });
    $scope.$on('$routeChangeSuccess', function() {
      $scope.loading = false;
    });
});