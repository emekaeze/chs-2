/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var url = 'https://api.mongolab.com/api/1/databases/sampledb/collections/persons?apiKey=9PentSceW9IxpK1Ho3d9K5rmvLw4TMLg';

compApp.controller('inboxCtrl',function($scope,$http){
										$(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		}
	} );

    yadcf.init(table , [{column_number : 4,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });
    
});

compApp.controller('discoTimelineCtrl',function($scope,$http){
	
});

compApp.controller('complaintFormCtrl',function($scope,$http){
												
	$scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
	
});

compApp.controller('parentController',function($scope){   
    
    $scope.loading = false;
    $scope.$on('$routeChangeStart', function() {
      $scope.loading = true;
    });
    $scope.$on('$routeChangeSuccess', function() {
      $scope.loading = false;
    });
});

compApp.controller('composeCtrl',function($scope,$http,$location){
    $scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
});
