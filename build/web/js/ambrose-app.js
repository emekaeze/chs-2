/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var mainApp = angular.module('mainApp',['ngRoute','ui.bootstrap']).
        config(['$routeProvider',function($routeProvider){
            $routeProvider.when('/dashboard', {templateUrl: 'dashboard.html', controller: 'dashboardCtrl'});            
            $routeProvider.when('/inbox', {templateUrl: 'inbox.html', controller: 'emptyCtrl'}); 
            $routeProvider.when('/gen_complaints', {templateUrl: 'gen_complaints.html', controller: 'genComplaintsCtrl'}); 
            $routeProvider.when('/morris-charts', {templateUrl: 'morris-charts.html', controller: 'emptyCtrl'}); 
            $routeProvider.when('/abuja_complaints', {templateUrl: 'abuja_complaints.html', controller: 'abujaComplaintsCtrl'}); 
            $routeProvider.when('/dashboard-eedc', {templateUrl: 'dashboard-eedc.html', controller: 'dashboardEedcCtrl'}); 
            $routeProvider.otherwise({redirectTo: '/dashboard', controller: 'dashboardCtrl'});
        }]);