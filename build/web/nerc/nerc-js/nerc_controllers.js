/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var url = 'https://api.mongolab.com/api/1/databases/sampledb/collections/persons?apiKey=9PentSceW9IxpK1Ho3d9K5rmvLw4TMLg';

mainApp.controller('loginCtrl',function($scope, $http){
    $scope.isError = false;
    $scope.isProcessing = false;    
    $scope.login = function(){
        $scope.isProcessing = true;        
        $http.get(url).success(function(persons){
            var isSuccessful = false;            
            for(var i=0;i<persons.length;i++){
                if($scope.username === persons[i].username){
                    if($scope.password===persons[i].password){
                        console.log(persons[i].path);                                                
                        isSuccessful = true;
                        window.location.href= persons[i].path;   
                        break;
                    }else{
                        break;
                    }                    
                }
            }
            if(!isSuccessful){
                console.log("Setting isError to true");
                $scope.isError = true;  
                $scope.isProcessing = false; 
            }
                       
        });      
    };
});

mainApp.controller('complaintFormCtrl',function($scope,$http){
    $scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
	
});
mainApp.controller('discoTimelineCtrl',function($scope,$http){
	
});

mainApp.controller('dashboardCtrl',function($scope,$http){
    $scope.$emit('LOAD');
    
        var chart1;
 // globally available 
  
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Complaints',
                x: -20 //center
            },
            subtitle: {
                text: 'For: All DISCO',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Complaint(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Complaints',
                data: [10, 69, 95, 145, 182, 215, 252, 265, 233, 183, 139, 96]
            }, {
                name: 'Resolved',
                data: [2, 57, 13, 120, 20, 28, 24, 20, 14, 6, 25,33]
            }, {
                name: 'Unresolved',
                data: [7, 35, 84, 135, 170, 180, 179, 143, 90, 39, 10, 54]
            }, {
                name: 'Pending',
                data: [5, 42, 57, 85, 19, 12, 170, 160, 142, 103, 66, 48]
            }]
        });   
		
	
   $scope.$emit('UNLOAD');
});
  
mainApp.controller('inboxCtrl',function($scope,$http){
								
        $scope.$emit('LOAD');                                                        
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		}
	} );

    yadcf.init(table , [{column_number : 4,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    $scope.$emit('UNLOAD');
   

});

mainApp.controller('nercTimelineCtrl',function($scope,$http){
	
});

mainApp.controller('emptyCtrl',function($scope,$http){        
   console.log('Calling the Empty controller') ;
});

mainApp.controller('genComplaintsCtrl',function($scope,$http){        
   $(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "media/swf/copy_csv_xls_pdf.swf"
		},
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }]
		
	} );

    yadcf.init(table , [

                    {column_number : 1, filter_type: "range_number_slider"},
                {column_number : 2, filter_type: "range_number_slider"},
                {column_number : 3, filter_type: "range_number_slider"},
                {column_number : 4, filter_type: "range_number_slider"},
                    {column_number : 5,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });

});

mainApp.controller('abujaComplaintsCtrl',function($scope,$http){        
   $(document).ready(function(){

    var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "media/swf/copy_csv_xls_pdf.swf"
		},
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }]
		
	} );
        yadcf.init(table , [

		{column_number : 1},
	    {column_number : 2},
	    {column_number : 3},
	    {column_number : 4},
		{column_number : 6,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });
});

mainApp.controller('dashboardEedcCtrl',function($scope,$http){
        
        var chart1;
 // globally available 
  $(document).ready(function () {
     $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Complaints',
                x: -20 //center
            },
            subtitle: {
                text: 'For: ENUGU DISCO',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Complaint(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Complaints',
                data: [0.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 2600.0]
            }, {
                name: 'Pending',
                data: [0.0, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 1000.0]
            }, {
				 name: 'Resolved',
                 data: [0.0, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5,600.0]
                
            }, {
                name: 'Unresolved',
                data: [0.0, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0, 1000.0]
				
            }]
        });
   });
});

});

mainApp.controller('dashboardEkodcCtrl',function($scope,$http){
        
     
});

mainApp.controller('dashboardIbedcCtrl',function($scope,$http){
        
        var chart1;
 // globally available 
  $(document).ready(function () {
     $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Complaints',
                x: -20 //center
            },
            subtitle: {
                text: 'For: IBADAN DISCO',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Complaint(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Complaints',
                data: [3000, 6000, 9, 1400, 18, 21000, 25, 2006, 233, 1800, 1300, 900000]
            }, {
                
                name: 'Pending',
                data: [90000, 420, 570, 800000, 110, 150, 170, 160, 140, 100, 60, 4000]
            }, {
			name: 'Resolved',
                data: [1000, 5, 11000, 17, 22000, 24, 24, 2000, 14, 80, 250,30000]
            }, {
			
				
                name: 'Unresolved',
                data: [60000, 3000, 840, 13000, 170, 1800, 179, 143, 900, 390, 100, 500000]
        
            }]
        });
	});
    
 });
     
});

mainApp.controller('dashboardIkedcCtrl',function($scope,$http){
        
     
});

mainApp.controller('dashboardPhcnCtrl',function($scope,$http){
        
     
});

mainApp.controller('dashboardPhedCtrl',function($scope,$http){
        
          var chart1;
 // globally available 
 $(document).ready(function () {
     $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Complaints',
                x: -20 //center
            },
            subtitle: {
                text: 'For: PORTHARCOURT DISCO',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Complaint(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Complaints',
                data: [10.0, 690, 950, 1450, 1820, 2150, 2520, 2650, 2330, 1830, 1390, 960]
            }, {
				name: 'Pending',
                data: [0, 42, 57, 85, 119, 152, 170, 166, 142, 103, 66, 48]
                
            }, {
                name: 'Resolved',
                data: [4, 57, 113, 170, 220, 248, 241, 201, 141, 86, 25,33]
				
            }, {
                name: 'Unresolved',
                data: [0, 35, 84, 135, 170, 186, 179, 143, 90, 39, 10, 54]
            }]
        });
    });
    
 });
 
    
});

mainApp.controller('genComplaintsCtrl',function($scope,$http){        
   $(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		}//,
		//"aoColumnDefs": [{ "bVisible": false, "aTargets": [5] }]
		
	} );

    yadcf.init(table , [

                    {column_number : 1, filter_type: "range_number_slider"},
                {column_number : 2, filter_type: "range_number_slider"},
                {column_number : 3, filter_type: "range_number_slider"},
                {column_number : 4, filter_type: "range_number_slider"},
                    {column_number : 5,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });

});

mainApp.controller('complaintsCatCtrl',function($scope,$http){        
   $(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		},
		sScrollX:true,
		"aoColumnDefs": [{ "bVisible": false, "aTargets": [11] }]
		
	} );

    yadcf.init(table , [
/*
                {column_number : 1},
                {column_number : 2},
                {column_number : 3},
                {column_number : 4},*/
                {column_number : 11,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });

});


mainApp.controller('leftBarCtrl',function($scope,$http){
   console.log('Header console has just been called') ;
});

mainApp.controller('parentController',function($scope){   
    
    $scope.loading = false;
    $scope.$on('$routeChangeStart', function() {
      $scope.loading = true;
    });
    $scope.$on('$routeChangeSuccess', function() {
      $scope.loading = false;
    });
});

function DropdownCtrl($scope) {

  $scope.status = {
    isopen: false
  };

  $scope.toggled = function(open) {
    console.log('Dropdown is now: ', open);
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };
}