$(document).ready(function () {
  //set false for non-ajax version
    var  ajax_version=true;

	$.ajaxSetup({
		cache: false
	});
		
	// After page load, basic jq functions
    $('.options-toggle').on('click', function (e) {
    	e.preventDefault();
    	$('.options-holder').toggleClass('closed');
        //$('.options-holder .col-sm-4').slideToggle('hidden');
      });

    $(".submenu > .dropdown").click(function(){
											 
 	if(false == $(this).next().is(':visible')) {
        $('.nav-list ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
    });

	 init();
	   
     var type = window.location.hash.substr(1);
    localStorage.setItem('working', type ) ;
		
      $('ul.nav-list li a').on('click', function (e) {
    	 var container = $('.main-content');
		// container.mask('<h1><i class="fa  fa-refresh fa-spin"></i> Loading...</h1>');
         
		 $('ul.nav-list li ').removeClass('active');
          $(this).parent().addClass('active');
          $('.user-details').addClass('user-details-close');
          
          if ($('.left-sidebar').hasClass('left-sidebar-open')) 
          {
          $('.left-sidebar').toggleClass('left-sidebar-open');
        }
        if ($('.site-holder').hasClass('mini-sidebar')) 
        {
          $('ul.nav-list li ul ').css('display','none');
      }
        //$('.options-holder .col-sm-4').slideToggle('hidden');
      });

$('ul.nav.only').on('click', function (e) {
    	 var container = $('.main-content');
		 container.mask('<h1><i class="fa  fa-refresh fa-spin"></i> Loading...</h1>');
         
		 $('ul.nav-list li ').removeClass('active');
          $(this).parent().addClass('active');
          $('.user-details').addClass('user-details-close');
          
          if ($('.left-sidebar').hasClass('left-sidebar-open')) 
          {
          $('.left-sidebar').toggleClass('left-sidebar-open');
        }
        if ($('.site-holder').hasClass('mini-sidebar')) 
        {
          $('ul.nav-list li ul ').css('display','none');
      }
        //$('.options-holder .col-sm-4').slideToggle('hidden');
      });


function init() {
	// PANELS
	  // panel close
        $('.panel-close').on('click', function (e) {
        	e.preventDefault();
        	$(this).parent().parent().parent().parent().addClass(' animated fadeOutDown');
        });

        //Todo List
        $('.finish').click(function(){
          $(this).parent().toggleClass('finished');
          $(this).toggleClass('fa-square-o');
        });
			
		//Inside Page
		
		
        $('.progress .progress-bar').progressbar(); 
        $('.fa-hover').click(function(e){
          e.preventDefault();
          var valued= $(this).find('i').attr('class');
          $('.modal-title').html(valued);
          $('.icon-show').html('<i class="' + valued + ' fa-5x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-4x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-3x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-2x "></i>&nbsp;&nbsp;<i class="' + valued + ' "></i>&nbsp;&nbsp;');
          $('.modal-footer span.icon-code').html('"' + valued + '"');
          $('#myModal').modal('show');
        });

	$('.panel-settings').click(function(e){
		e.preventDefault();
		var columnId=$(this).closest(".panel").parent().attr("id");
		var title=$(this).closest(".panel-title").text();
		//var valued= $(this).find('i').attr('class');
		//$('.modal-title').html(valued);
		//var vj=$(this).parent().parent().parent().parent().parent().getElementById();
		$('.modal-title').html(title);
		$('#customiseWidget #current-div').val(columnId);
		$('#customiseWidget #column-size').val($('#'+columnId).attr('class'));
		//$('.modal-footer span.icon-code').html('"' + valued + '"');
		$('#customiseWidget').modal('show');
	});

	$('#customiseWidget #submit').on('click',function(e)
      {
    		
    		e.preventDefault();
    		var color=$('#customiseWidget #color').val();
    		var title=$('#customiseWidget #title').val();
    		var targetDiv='#'+$('#customiseWidget #current-div').val();
    		var columnSize=$('#customiseWidget #column-size').val();
	    	$(targetDiv).find('.panel').attr('class','panel '+color);
	    	$(targetDiv).attr('class',columnSize);
		$('#customiseWidget').modal('hide');
	});

        $('.panel-minimize').on('click', function (e) 
        {
        	e.preventDefault();
        	var $target = $(this).parent().parent().parent().next('.panel-body');
        	if ($target.is(':visible')) {
        		$('i', $(this)).removeClass('fa-chevron-up').addClass('fa-chevron-down');
        	} else {
        		$('i', $(this)).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        	}
        	$target.slideToggle();
        });
        
        
        $('.panel-refresh').on('click', function (e) 
        {
        	e.preventDefault();
        	var $target = $(this).parent().parent().parent().next('.panel-body');
        	$target.mask('<i class="fa fa-refresh fa-spin"></i> Loading...');

        	setTimeout(function () {
        		$target.unmask();

        	},
        	1000);
        });
      }
	  
	  function sortablePortlets()
{
	$(".grid").sortable({
		//tolerance: 'pointer',
		revert: 'invalid',
		cursor: 'move',
		placeholder: ' col-md-2 well placeholder tile',
            handle: ".panel-heading",
            forceHelperSize: true,
            start: function(e, ui){
            	ui.placeholder.height(ui.helper.height())-15;
            	ui.placeholder.width(ui.helper.width()-15);
            },
            stop: function(evt, ui){
            	//console.log($(".grid").sortable('toArray', { attribute: 'data-name' }));
            }
          });

	$( ".grid" ).on("sortupdate",function( event, ui ) {
		var sorted = $( this ).sortable( "serialize");
		//console.log(sorted);
		localStorage.setItem('sorted', sorted) ;

	});
	if(localStorage.getItem("sorted") !== null){
		var arrValuesForOrder = localStorage.getItem('sorted').substring(6).split("&div[]="); 

		var $ul = $(".grid");
		$items = $(".grid").children();

// loop backwards so you can just prepend elements in the list
// instead of trying to place them at a specific position
for (var i = arrValuesForOrder[arrValuesForOrder.length - 1]; i >= 0; i--) {
    // index is zero-based to you have to remove one from the values in your array
    $ul.prepend( $items.get((arrValuesForOrder[i] - 1)));
  		}
	}

}

});