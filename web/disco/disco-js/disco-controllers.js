/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


compApp.controller('dashboardCtrl',function($scope,$http){
	$(document).ready(function(){

				var table = $('#example').DataTable( {
						dom: '<"top"fl>rt<T>rt<"bottom"ip><"clear">',
						tableTools: {
							"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
						},
						ordering: true,
						"iDisplayLength": 5,
						"aoColumnDefs": [{ "bVisible": false, "aTargets": [3] }]
						
					} );
	
					yadcf.init(table , [{column_number : 1},
										{column_number : 2},
										{column_number : 3,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
					});
					
    
});

compApp.controller('complaintFormCtrl',function($scope,$http){
												
	$scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           

    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
	
});

compApp.controller('discoUploadCtrl',function($scope,$http){
	
});
compApp.controller('discoTimelineCtrl',function($scope,$http){
	

});

compApp.controller('inboxCtrl',function($scope,$http){
										
	$(document).ready(function() {
	var table = $('#example').DataTable( {
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../media/swf/copy_csv_xls_pdf.swf"
		}
	} );

    yadcf.init(table , [{column_number : 4,  filter_type: "range_date", filter_container_id: "external_filter_container"}]);
    });

});


compApp.controller('dashboardEedcCtrl',function($scope,$http){
        
        var chart1;
 // globally available 
  $(document).ready(function () {
     $(function () {
        $('#container').highcharts({
            title: {
                text: 'Monthly Average Complaints',
                x: -20 //center
            },
            subtitle: {
                text: 'For: ENUGU DISCO',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Complaints'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Complaint(s)'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Total Complaints',
                data: [0.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 2600.0]
            }, {
                name: 'Pending',
                data: [0.0, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 1000.0]
            }, {
				 name: 'Resolved',
                 data: [0.0, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5,600.0]
                
            }, {
                name: 'Unresolved',
                data: [0.0, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0, 1000.0]
				
            }]
        });
   });
});

});


compApp.controller('composeCtrl',function($scope,$http,$location){
    $scope.isBusy = false;
    $scope.isShowMoreAttachments = false;
    $scope.showMoreLabel = 'Show more';
    
    $scope.send = function(){
        $scope.isBusy = true;
        $http.get(url).success(function(persons){
           alert("The complaint has been sent");
           $location.path('/inbox');
           $scope.isBusy = false;
         });           
    };
    $scope.showMoreBoxes = function(){
        $scope.isShowMoreAttachments = !$scope.isShowMoreAttachments;        
    };
    
});

compApp.controller('parentController',function($scope){   
    
    $scope.loading = false;
    $scope.$on('$routeChangeStart', function() {
      $scope.loading = true;
    });
    $scope.$on('$routeChangeSuccess', function() {
      $scope.loading = false;
    });
});