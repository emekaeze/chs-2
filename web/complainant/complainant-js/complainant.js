/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var compApp = angular.module('compApp',['ngRoute','ui.bootstrap']).
config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/inbox', {templateUrl: '../complainant/inbox.html', controller: 'inboxCtrl'});            
    $routeProvider.when('/compose', {templateUrl: '../complainant/compose.html', controller: 'composeCtrl'});  
    $routeProvider.when('/profile', {templateUrl: '../complainant/profile.html', controller: 'profileCtrl'});   
	$routeProvider.when('/timeline3', {templateUrl: '../complainant/timeline3.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline4', {templateUrl: '../complainant/timeline4.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline5', {templateUrl: '../complainant/timeline5.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline6', {templateUrl: '../complainant/timeline6.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/complaint-form', {templateUrl: '../complainant/complaint-form.html', controller: 'complaintFormCtrl'});
    $routeProvider.otherwise({redirectTo: '/complaint-form'});

}]);

