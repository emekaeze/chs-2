/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var compApp = angular.module('compApp',['ngRoute','ui.bootstrap']).
config(['$routeProvider',function($routeProvider){
	$routeProvider.when('/dashboard', {templateUrl: '../forum/dashboard.html', controller: 'dashboardForumCtrl'});
	$routeProvider.when('/inbox', {templateUrl: '../forum/inbox.html', controller: 'inboxCtrl'});  
	$routeProvider.when('/timeline', {templateUrl: '../forum/timeline.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/complaint-form-disco', {templateUrl: '../forum/complaint-form-disco.html', controller: 'complaintFormCtrl'});
	$routeProvider.when('/timeline3', {templateUrl: '../forum/timeline3.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline4', {templateUrl: '../forum/timeline4.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline5', {templateUrl: '../forum/timeline5.html', controller: 'discoTimelineCtrl'});
	$routeProvider.when('/timeline6', {templateUrl: '../forum/timeline6.html', controller: 'discoTimelineCtrl'});
        $routeProvider.otherwise({redirectTo:'/dashboard'});
}]);

