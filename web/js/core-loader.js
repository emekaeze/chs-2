/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    
var container = $('.main-content');
var type = window.location.hash.substr(1);
localStorage.setItem('working', type ) ;


if(ajax_version)
{
      setTimeout(function () {
            loadPage(type, container)
                $('ul.nav-list li:has(a[href="' + type + '"])').addClass('active').closest('.submenu').addClass('current').find('ul').css('display','block');
        },
      2000);
}

//input search function
$('.nav-input-search').typeahead({
     source: [
        { id: 'dashboard', name: 'dashboard' },
        { id: 'dashboard', name: 'Chat' },
        { id: 'gallery', name: 'Gallery' },
        { id: 'gallery', name: 'Images' },
        { id: 'dashboard', name: 'Calendar' },
        { id: 'portlets', name: 'Portlets' },
        { id: 'delighted-layouts', name: 'Delighted Layouts' },
        { id: 'grids', name: 'Grids' },        
        { id: 'template', name: 'Template Page' },
        { id: 'template', name: 'Basic Template' },
        { id: 'inbox', name: 'Inbox' },
        { id: 'invoice', name: 'Invoice' },       
        { id: 'typography', name: 'Typography' },
        { id: 'tasks', name: 'Taks' },
        { id: 'alerts', name: 'Alerts' },
        { id: 'profile', name: 'Profile' },
        { id: 'delighted-wiki', name: 'Delighted Wiki' },
        { id: 'buttons', name: 'Buttons' },
        { id: 'panels', name: 'Panels' },
        { id: 'jquery-ui', name: 'Sliders' },
        { id: 'maps', name: 'Maps' },
        { id: 'timeline', name: 'timeline' },
        { id: 'flot-charts', name: 'Flot Charts' },
        { id: 'morris-charts', name: 'Morris Charts' },
        { id: 'inline-charts', name: 'Inline Charts' },
        { id: 'nvd3charts', name: 'NVD3 Charts' },
        { id: 'chartjs', name: 'Chartjs' },
        { id: 'date-paginator', name: 'Date Paginator' },
        { id: 'portlets', name: 'Draggable Portlets' },
        { id: 'jq-gridplugin', name: 'Jq grid Plugin' },
        { id: 'simple-tables', name: 'Simple tables' },
        { id: 'dynamic-tables', name: 'Dynamic tables' },
        { id: 'dropzone-file-upload', name: 'Dropzone file upload' },
        { id: 'form-input-masks', name: 'Form input masks' },
        { id: 'font-awesome', name: 'Font Awesome' },
        { id: 'input-groups', name: 'Input groups' },
        { id: 'ionicons', name: 'Ion Icons' },
        { id: 'inbox', name: 'Mail' },
        { id: 'jquery-ui', name: 'Color picker' },
        { id: 'social', name: 'Social' },
        { id: 'filetypes', name: 'File Types' },
        { id: 'glyphicons', name: 'Glyphicons' },
        { id: 'editor', name: 'Wysiwyg Editor' },
        { id: 'grid', name: 'Grid' },
        { id: 'file-manager', name: 'File Manager' },
        { id: 'image-crop', name: 'Image Crop' },
        { id: 'dropzone', name: 'Dropzone' },
        { id: 'bootstrap-editors', name: 'Bootstrap Editors' },
        { id: 'wizards', name: 'wizards' },
        { id: 'form-elements', name: 'Form Validations' },
        { id: 'form-layouts', name: 'Form Layouts' },
        { id: 'form-elements', name: 'Form Elements' },
        { id: 'dashboard', name: 'Docs' },
        { id: 'dashboard', name: 'Unlimited Menu' },
        { id: 'general-elements', name: 'General Elements' },
        { id: 'tree-view', name: 'Tree View' },
        { id: 'jquery-ui', name: 'Jquery Ui' },
        { id: 'nestable-lists', name: 'Nestabl Lists' },
        { id: 'extended-zoom', name: 'Extended Zoom' },
        { id: 'dropzone', name: 'Drop Zone' },
        { id: 'editor', name: 'Wysiwyg' },
        { id: 'editor', name: 'Bootstrap Editor' },

      ],
        itemSelected: displayResult
  });

function loadPage(url, container) {
      urlExt = url + '.html';
        //console.log(container)
        $.ajax({
        	type: "GET",
        	url: urlExt,
        	dataType: 'html',
        	cache: false,
        	success: function () {
        		container.mask('<h1><i class="fa  fa-refresh fa-spin"></i> Loading...</h1>');
        		container.load(urlExt, null, function (responseText) {
                  window.location.hash =url;
                  $('.breadcrumb .active').html(url);

    			init();
    			sortablePortlets();
        		}).fadeIn('slow');
        		//console.log("ajax request successful");
        	},
        	error: function (xhr, ajaxOptions, thrownError) {
        		//container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
                        container.load('404.html') ;
                        setTimeout(function () {
                        loadPage('dashboard', container)
                                  $('ul.nav-list li ').removeClass('active');
                                $('ul.nav-list li:has(a[href="dashboard"])').addClass('active').closest('.submenu').addClass('current').find('ul').css('display','block');
                          },
                    3000);
              },
              async: false
            });
     }

//left side bar search box
function displayResult(item, val, text) 
{
    var container = $('.main-content');
          if(ajax_version)
          {
            loadPage(val, container);
            $('.nav-input-search').val('')
          }
          else
          {
            window.href.location="val"
          }
} 
