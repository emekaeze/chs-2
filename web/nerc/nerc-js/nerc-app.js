/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var mainApp = angular.module('mainApp',['ngRoute','ui.bootstrap']).
        config(['$routeProvider',function($routeProvider){
            $routeProvider.when('/dashboard', {templateUrl: '../nerc/dashboard.html', controller: 'dashboardCtrl'});
            $routeProvider.when('/dashboard-phedc', {templateUrl: '../nerc/dashboard-phedc.html', controller: 'dashboardPhedCtrl'});
            $routeProvider.when('/dashboard-ekodc', {templateUrl: '../nerc/dashboard-ekodc.html', controller: 'dashboardEkodcCtrl'});
            $routeProvider.when('/dashboard-ibedc', {templateUrl: '../nerc/dashboard-ibedc.html', controller: 'dashboardIbedcCtrl'});
            $routeProvider.when('/dashboard-ikedc', {templateUrl: '../nerc/dashboard-ikedc.html', controller: 'dashboardIkedcCtrl'});
            $routeProvider.when('/dashboard-eedc', {templateUrl: '../nerc/dashboard-eedc.html', controller: 'dashboardEedcCtrl'});
            $routeProvider.when('/dashboard-phcn', {templateUrl: '../nerc/dashboard-phcn.html', controller: 'dashboardPhcnCtrl'});
            $routeProvider.when('/gen-complaints', {templateUrl: '../nerc/gen-complaints.html', controller: 'genComplaintsCtrl'}); 
            $routeProvider.when('/abuja_complaints', {templateUrl: '../nerc/abuja_complaints.html', controller: 'abujaComplaintsCtrl'}); 
            $routeProvider.when('/complaint-category', {templateUrl: '../nerc/complaint-category.html', controller: 'complaintsCatCtrl'}); 
            $routeProvider.when('/inbox', {templateUrl: '../nerc/inbox.html', controller: 'inboxCtrl'}); 
            $routeProvider.when('/timeline', {templateUrl: 'timeline.html', controller: 'nercTimelineCtrl'});
            $routeProvider.when('/timeline3', {templateUrl: '../nerc/timeline3.html', controller: 'nercTimelineCtrl'});
            $routeProvider.when('/timeline4', {templateUrl: '../nerc/timeline4.html', controller: 'nercTimelineCtrl'});
            $routeProvider.when('/timeline5', {templateUrl: '../nerc/timeline5.html', controller: 'nercTimelineCtrl'});
            $routeProvider.when('/timeline6', {templateUrl: '../nerc/timeline6.html', controller: 'nercTimelineCtrl'});
            $routeProvider.when('/complaint-form-nerc', {templateUrl: '../nerc/complaint-form-nerc.html', controller: 'complaintFormCtrl'});
            $routeProvider.otherwise({redirectTo: '/dashboard'});
        }]);